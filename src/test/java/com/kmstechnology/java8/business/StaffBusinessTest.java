package com.kmstechnology.java8.business;

import com.kmstechnology.java8.entity.Staff;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class StaffBusinessTest {

    StaffBusiness staffBusiness;
    Staff staff;

    @Before
    public void setUp ()
    {
        staffBusiness = new StaffBusiness();

        String[] data = {
                "Mickey",
                "",
                "Mouse",
                "2000-01-01",
                "M",
                "001-002-003",
                "The Walt Disney Company"
        };
        staff = new Staff(data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
    }

    @Test
    public void testValidateStaffByBusiness() {
        boolean result = StaffBusiness.validateStaffByBusiness(null);
        Assert.assertFalse(result);

        result = StaffBusiness.validateStaffByBusiness(staff);
        Assert.assertTrue(result);
    }

    @Test
    public void testValidateEntity() {
        boolean result = staffBusiness.validateEntity(null);
        Assert.assertFalse(result);

        result = staffBusiness.validateEntity(staff);
        Assert.assertTrue(result);
    }
}