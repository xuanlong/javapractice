package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;


public class NonEmptyValidatorTest {

    TextValidator nonEmptyValidator = new NonEmptyValidator();

    @Test
    public void testValidateText() {
        String text = "";
        boolean result = nonEmptyValidator.validateText(text);
        Assert.assertFalse(result);

        text = "  ";
        result = nonEmptyValidator.validateText(text);
        Assert.assertTrue(result);
    }
}