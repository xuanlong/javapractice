package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;


public class NonNumberValidatorTest {
    TextValidator nonNumberValidator = new NonNumberValidator();

    @Test
    public void testValidateText() {
        String text = "22";
        boolean result = nonNumberValidator.validateText(text);
        Assert.assertFalse(result);

        text = "5.05";
        result = nonNumberValidator.validateText(text);
        Assert.assertFalse(result);

        text = "-200";
        result = nonNumberValidator.validateText(text);
        Assert.assertFalse(result);

        text = "10.0d";
        result = nonNumberValidator.validateText(text);
        Assert.assertFalse(result);

        text = "   222     ";
        result = nonNumberValidator.validateText(text);
        Assert.assertFalse(result);

        text = "";
        result = nonNumberValidator.validateText(text);
        Assert.assertTrue(result);

        text = "abc";
        result = nonNumberValidator.validateText(text);
        Assert.assertTrue(result);
    }
}