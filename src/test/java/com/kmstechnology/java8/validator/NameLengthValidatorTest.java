package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class NameLengthValidatorTest {

    TextValidator nameLengthValidator = new NameLengthValidator();

    @Test
    public void testValidateText() {
        String text = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        boolean result = nameLengthValidator.validateText(text);
        Assert.assertFalse(result);

        text = "aaaaa";
        result = nameLengthValidator.validateText(text);
        Assert.assertTrue(result);
    }
}