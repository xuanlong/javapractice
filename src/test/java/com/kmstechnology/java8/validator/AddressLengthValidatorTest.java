package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;


public class AddressLengthValidatorTest {
    TextValidator addressLengthValidator = new AddressLengthValidator();

    @Test
    public void testValidateText() {
        String text = "abc xyz";
        boolean result = addressLengthValidator.validateText(text);
        Assert.assertTrue(result);

        text = "abc                                                                                            "
                + "                                                                                            "
                + "                                                                                            "
                + "                                                                                            "
                + "                                                                                            "
                + "                                                                                            "
                + "                                                                                            "
                + "                                                                                            ";
        result = addressLengthValidator.validateText(text);
        Assert.assertFalse(result);
    }
}