package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;


public class PhoneFormatValidatorTest {
    TextValidator phoneFormatValidator = new PhoneFormatValidator();

    @Test
    public void testValidateText() {
        String text = "XXX-XXX-XXX";
        boolean result = phoneFormatValidator.validateText(text);
        Assert.assertFalse(result);

        text = "123-456-789";
        result = phoneFormatValidator.validateText(text);
        Assert.assertTrue(result);

        text = "123-xyz-789";
        result = phoneFormatValidator.validateText(text);
        Assert.assertFalse(result);

        text = "123-456-789-345";
        result = phoneFormatValidator.validateText(text);
        Assert.assertFalse(result);
    }
}