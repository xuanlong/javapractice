package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;

public class AddressFormatValidatorTest {

    TextValidator addressFormatValidator = new AddressFormatValidator();
    @Test
    public void testValidateText() {
        String text = "Walt Disney Company";
        boolean result = addressFormatValidator.validateText(text);
        Assert.assertTrue(result);

        text = "           ";
        result = addressFormatValidator.validateText(text);
        Assert.assertFalse(result);

        text = "        ";
        result = addressFormatValidator.validateText(text);
        Assert.assertFalse(result);

        text = "abc@xyz - 123";
        result = addressFormatValidator.validateText(text);
        Assert.assertTrue(result);
    }
}