package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;

public class GenderValidatorTest {

    TextValidator genderValidator = new GenderValidator();

    @Test
    public void testValidateText() {
        String text = "";
        boolean result = genderValidator.validateText(text);
        Assert.assertFalse(result);

        text = "M";
        result = genderValidator.validateText(text);
        Assert.assertTrue(result);

        text = "F";
        result = genderValidator.validateText(text);
        Assert.assertTrue(result);

        text = "m";
        result = genderValidator.validateText(text);
        Assert.assertFalse(result);

        text = "mf";
        result = genderValidator.validateText(text);
        Assert.assertFalse(result);
    }
}