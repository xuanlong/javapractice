package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;


public class DateFormatValidatorTest {
    TextValidator dateFormatValidator = new DateFormatValidator();
    @Test
    public void testValidateText() {
        String date = "2000-01-01";
        boolean result = dateFormatValidator.validateText(date);
        Assert.assertTrue(result);

        date = "20000-12-01";
        result = dateFormatValidator.validateText(date);
        Assert.assertFalse(result);

        date = "2000-31-01";
        result = dateFormatValidator.validateText(date);
        Assert.assertFalse(result);

        date = "2000/01/01";
        result = dateFormatValidator.validateText(date);
        Assert.assertFalse(result);

        date = "2000/31/01";
        result = dateFormatValidator.validateText(date);
        Assert.assertFalse(result);
    }
}