package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.junit.Assert;
import org.junit.Test;


public class CharactersValidatorTest {
    TextValidator charactersValidator = new CharactersValidator();
    @Test
    public void testValidateText() {
        String text = "Nguyen";
        boolean result = charactersValidator.validateText(text);
        Assert.assertTrue(result);

        text = "Nguyen5";
        result = charactersValidator.validateText(text);
        Assert.assertTrue(result);

        text = "N guyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);

        text = "N   guyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);

        text = "@#Nguyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);

        text = "\\Nguyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);

        text = "N\"guyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);

        text = "N[guyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);

        text = "N]guyen";
        result = charactersValidator.validateText(text);
        Assert.assertFalse(result);
    }
}