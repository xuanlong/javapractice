package com.kmstechnology.java8.sql.query;

import org.junit.Assert;
import org.junit.Test;

public class InsertQueryTest {
    SqlQuery insertQuery = new InsertQuery();

    @Test
    public void testGenerateQueryString() {
        String table = "staff";
        String query = insertQuery.generateQueryString(table);
        String expectedQuery = "insert into staff (first_name, middle_name, last_name, date_of_birth, " +
                "gender, phone, address) values (?, ?, ?, ?, ?, ?, ?)";
        boolean result = expectedQuery.equals(query);
        Assert.assertTrue(result);
    }
}