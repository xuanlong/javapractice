package com.kmstechnology.java8.sql.table;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class TableTest {

    @Spy Table table;

    @Test
    public void testGetName() {
        given (table.getName()).willReturn("MyTable");

        String result = table.getName();

        assertThat(result, is("MyTable"));
    }

    @Test
    public void getColumns() {
        List<String> columns = new ArrayList<String>();
        columns.add("column1");
        columns.add("column2");

        given(table.getColumns()).willReturn(columns);
        List<String> result = table.getColumns();
        assertThat(result, hasItems("column1", "column2"));
    }
}