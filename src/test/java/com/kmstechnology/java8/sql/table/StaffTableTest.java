package com.kmstechnology.java8.sql.table;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class StaffTableTest {
    StaffTable staffTable = new StaffTable();

    @Test
    public void testGetName (){
        String name = "staff";
        boolean result = staffTable.getName().equals(name);
        assertTrue(result);
    }

    @Test
    public void testGetColumns (){
        String[] columns = new String[]{
                "first_name",
                "middle_name",
                "last_name",
                "date_of_birth",
                "gender",
                "phone",
                "address"
        };
        List<String> staffColumns = staffTable.getColumns();

        boolean result = columns.length == staffColumns.size();
        assertTrue(result);

        result = true;
        for (int i = 0; i < columns.length; ++i) {
            if (!columns[i].equals(staffColumns.get(i))) {
                result = false;
                break;
            }
        }
        assertTrue(true);
    }
}