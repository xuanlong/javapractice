package com.kmstechnology.java8.sql.table;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DatabaseTableTest {

    @Test
    public void testGetColumnsFromTable() {
        String tableName = "staff";
        String[] expectedColumns = new String[]{
                "first_name",
                "middle_name",
                "last_name",
                "date_of_birth",
                "gender",
                "phone",
                "address"
        };

        List<String> actualColumns = DatabaseTable.getColumnsFromTable(tableName);
        boolean result = expectedColumns.length == actualColumns.size();
        Assert.assertTrue(result);

        result = true;
        for (int i = 0; i < expectedColumns.length; ++i) {
            if (!expectedColumns[i].equals(actualColumns.get(i))){
                result = false;
                break;
            }
        }
        Assert.assertTrue(result);
    }
}