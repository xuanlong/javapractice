package com.kmstechnology.java8.sql;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class SqlWrapperTest {
    SqlWrapper sqlWrapper;

    @Before
    public void setUp() {
        sqlWrapper = new SqlWrapper();
    }

    @Test
    public void testOpenConnection() {
        try {
            sqlWrapper.openConnection();
            boolean result = sqlWrapper.getConnection() != null;
            Assert.assertTrue(result);
        } catch (ClassNotFoundException e) {
            System.out.println(e.toString());
            Assert.assertTrue(false);
        } catch (SQLException e) {
            System.out.println(e.toString());
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testCloseConnection() {
        try {
            sqlWrapper.openConnection();
            sqlWrapper.closeConnection();
            boolean result = sqlWrapper.getConnection() == null;
            Assert.assertTrue(result);
        } catch (ClassNotFoundException e) {
            System.out.println(e.toString());
            Assert.assertTrue(false);
        } catch (SQLException e) {
            System.out.println(e.toString());
            Assert.assertTrue(false);
        }
    }
}