package com.kmstechnology.java8;

import com.kmstechnology.java8.entity.Staff;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class DemoAppTest {
    DemoApp app = new DemoApp();

    @Test
    public void testParseCSV() {
        String path = "staff.csv";
        List<Staff> staffs = app.parseCSV(path);
        Assert.assertTrue(staffs != null);
    }

    @Test
    public void testInsertStaffsIntoDatabase (){
        Map<Boolean, List<Staff>> staffsByPartition = mock(Map.class);
        List<Staff> expectation = new ArrayList<>();
        app.setPartitionStaffs(staffsByPartition);
        given(staffsByPartition.get(true)).willReturn(expectation);

        app.insertStaffsIntoDatabase();

        verify(staffsByPartition).get(true);
    }

    @Test
    public void testWriteLogsForFailRecords (){
        Map<Boolean, List<Staff>> staffsByPartition = mock(Map.class);
        List<Staff> expectation = new ArrayList<>();
        app.setPartitionStaffs(staffsByPartition);
        given(staffsByPartition.get(false)).willReturn(expectation);

        app.writeLogsForFailRecords();

        verify(staffsByPartition).get((false));
    }
}