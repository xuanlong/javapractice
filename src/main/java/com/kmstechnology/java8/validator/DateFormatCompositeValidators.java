package com.kmstechnology.java8.validator;

public class DateFormatCompositeValidators extends CompositeValidators {
    @Override
    public void addDefaultValidators() {
        addValidator(new NonEmptyValidator());
        addValidator(new DateFormatValidator());
    }

    @Override
    public String getValidatorName() {
        return DateFormatCompositeValidators.class.getName();
    }
}
