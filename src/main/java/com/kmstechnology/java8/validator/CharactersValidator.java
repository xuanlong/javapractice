package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

public class CharactersValidator implements TextValidator {
    private static final Pattern CHARACTERS_PATTERN = Pattern.compile("\\w*", Pattern.UNICODE_CHARACTER_CLASS);

    @Override
    public boolean validateText(@NotNull String text) {
        return CHARACTERS_PATTERN.matcher(text).matches();
    }

    @Override
    public String getValidatorName() {
        return CharactersValidator.class.getName();
    }
}
