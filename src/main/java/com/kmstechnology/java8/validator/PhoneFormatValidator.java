package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

public class PhoneFormatValidator implements TextValidator {
    private static final Pattern PHONE_PATTERN = Pattern.compile("\\d{3}-\\d{3}-\\d{3}");

    @Override
    public boolean validateText(@NotNull String text) {
        return PHONE_PATTERN.matcher(text).matches();
    }

    @Override
    public String getValidatorName() {
        return PhoneFormatValidator.class.getName();
    }
}
