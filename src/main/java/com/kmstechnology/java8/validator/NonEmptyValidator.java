package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

public class NonEmptyValidator implements TextValidator {
    @Override
    public boolean validateText(@NotNull String text) {
        return text.length() > 0;
    }

    @Override
    public String getValidatorName() {
        return NonEmptyValidator.class.getName();
    }
}
