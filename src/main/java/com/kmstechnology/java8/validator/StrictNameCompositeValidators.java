package com.kmstechnology.java8.validator;

public class StrictNameCompositeValidators extends CompositeValidators {

    @Override
    public String getValidatorName() {
        return StrictNameCompositeValidators.class.getName();
    }

    @Override
    public void addDefaultValidators() {
        addValidator(new NonEmptyValidator());
        addValidator(new NameLengthValidator());
        addValidator(new NonNumberValidator());
        addValidator(new CharactersValidator());
    }
}
