package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class ObjectValidator {
    Map<String, TextValidator> validatorByText;

    public ObjectValidator() {
        validatorByText = new HashMap<>();
    }

    public void addValidator(TextValidator validator) {
        if (validator != null
                && !validatorByText.containsKey(validator.getValidatorName()))
            validatorByText.put(validator.getValidatorName(), validator);
    }

    public boolean validateTextByName(@NotNull String text, @NotNull String validatorName) {
        return validatorByText.get(validatorName).validateText(text);
    }
}
