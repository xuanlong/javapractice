package com.kmstechnology.java8.validator;

public class AddressCompositeValidators extends CompositeValidators {
    @Override
    public void addDefaultValidators() {
        addValidator(new NonEmptyValidator());
        addValidator(new AddressLengthValidator());
        addValidator(new NonNumberValidator());
        addValidator(new AddressFormatValidator());
    }

    @Override
    public String getValidatorName() {
        return AddressCompositeValidators.class.getName();
    }
}
