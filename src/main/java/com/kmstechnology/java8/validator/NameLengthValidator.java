package com.kmstechnology.java8.validator;

import com.kmstechnology.java8.validator.TextValidator;
import org.jetbrains.annotations.NotNull;

public class NameLengthValidator implements TextValidator {
    private static final int MAX_NAME_LENGTH = 50;

    @Override
    public boolean validateText(@NotNull String text) {
        return text.length() <= MAX_NAME_LENGTH;
    }

    @Override
    public String getValidatorName() {
        return NameLengthValidator.class.getName();
    }
}
