package com.kmstechnology.java8.validator;

public class GenderCompositeValidators extends CompositeValidators {
    @Override
    public void addDefaultValidators() {
        addValidator(new NonEmptyValidator());
        addValidator(new GenderValidator());
    }

    @Override
    public String getValidatorName() {
        return GenderCompositeValidators.class.getName();
    }
}
