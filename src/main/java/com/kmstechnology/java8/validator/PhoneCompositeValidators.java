package com.kmstechnology.java8.validator;

public class PhoneCompositeValidators extends CompositeValidators {
    @Override
    public void addDefaultValidators() {
        addValidator(new NonEmptyValidator());
        addValidator(new PhoneFormatValidator());
    }

    @Override
    public String getValidatorName() {
        return PhoneCompositeValidators.class.getName();
    }
}
