package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

public class GenderValidator implements TextValidator {
    @Override
    public boolean validateText(@NotNull String text) {
        return text.equals("M") || text.equals("F");
    }

    @Override
    public String getValidatorName() {
        return GenderValidator.class.getName();
    }
}
