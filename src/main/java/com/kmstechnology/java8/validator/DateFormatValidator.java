package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateFormatValidator implements TextValidator {
    @Override
    public boolean validateText(@NotNull String text) {
        try {
            LocalDate.parse(text, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    @Override
    public String getValidatorName() {
        return DateFormatValidator.class.getName();
    }
}
