package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

public class AddressFormatValidator implements TextValidator {
    @Override
    public boolean validateText(@NotNull String text) {
        boolean result = text.chars().allMatch(Character::isWhitespace);
        return !result;
    }

    @Override
    public String getValidatorName() {
        return AddressFormatValidator.class.getName();
    }
}
