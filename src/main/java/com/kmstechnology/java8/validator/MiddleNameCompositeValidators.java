package com.kmstechnology.java8.validator;

public class MiddleNameCompositeValidators extends CompositeValidators {
    @Override
    public void addDefaultValidators() {
        addValidator(new NameLengthValidator());
        addValidator(new NonNumberValidator());
        addValidator(new CharactersValidator());
    }

    @Override
    public String getValidatorName() {
        return MiddleNameCompositeValidators.class.getName();
    }
}
