package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

public interface TextValidator {
    boolean validateText(@NotNull String text);

    String getValidatorName();
}
