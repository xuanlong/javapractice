package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

public class NonNumberValidator implements TextValidator {
    @Override
    public boolean validateText(@NotNull String text) {
        try {
            Double.parseDouble(text);
        } catch (NumberFormatException e) {
            return true;
        }
        return false;
    }

    @Override
    public String getValidatorName() {
        return NonNumberValidator.class.getName();
    }
}
