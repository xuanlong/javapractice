package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public abstract class CompositeValidators implements TextValidator {
    protected List<TextValidator> validators;

    public CompositeValidators() {
        validators = new ArrayList<>();
    }

    public abstract void addDefaultValidators();

    public void addValidator(TextValidator validator) {
        if (validator != null)
            validators.add(validator);
    }

    public void removeValidator(TextValidator validator) {
        if (validator != null)
            validators.remove(validator);
    }

    @Override
    public boolean validateText(@NotNull String text) {
        return validators.stream()
                .allMatch(validator -> validator.validateText(text));
    }
}
