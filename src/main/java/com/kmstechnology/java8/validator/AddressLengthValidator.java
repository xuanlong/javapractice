package com.kmstechnology.java8.validator;

import org.jetbrains.annotations.NotNull;

public class AddressLengthValidator implements TextValidator {
    private static final int MAX_LENGTH = 500;

    @Override
    public boolean validateText(@NotNull String text) {
        return text.length() <= MAX_LENGTH;
    }

    @Override
    public String getValidatorName() {
        return AddressLengthValidator.class.getName();
    }
}
