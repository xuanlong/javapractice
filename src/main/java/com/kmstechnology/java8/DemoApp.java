package com.kmstechnology.java8;

import com.kmstechnology.java8.business.StaffBusiness;
import com.kmstechnology.java8.entity.Staff;
import com.kmstechnology.java8.sql.SqlWrapper;
import com.kmstechnology.java8.sql.query.SqlQueryGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DemoApp {
    private static final String STAFF_CSV = "staff.csv";
    Map<Boolean, List<Staff>> partitionStaffs;

    public static void main(String[] args) {
        DemoApp app = new DemoApp();
        Optional<List<Staff>> staffs;

        staffs = Optional.ofNullable(app.parseCSV(STAFF_CSV));

        staffs.ifPresent(s -> app.setPartitionStaffs(s.stream().parallel().collect(
                Collectors.partitioningBy(StaffBusiness::validateStaffByBusiness)
        )));

        Thread insertStaffsThread = new Thread(app::insertStaffsIntoDatabase);
        Thread logFailRecordsThread = new Thread(app::writeLogsForFailRecords);
        insertStaffsThread.start();
        logFailRecordsThread.start();
    }

    public List<Staff> parseCSV(String path) {
        List<Staff> staffs = null;
        try (BufferedReader csvReader = new BufferedReader(new FileReader(path));) {
            staffs = new ArrayList<>();
            // skip the first line for reading titles:
            // firstName, middleName, lastName, ...
            String row = csvReader.readLine();
            while ((row = csvReader.readLine()) != null) {
                String[] fields = row.split(",", -1);
                Staff s = new Staff(fields);
                staffs.add(s);
            }
        } catch (IOException e) {
            System.out.println(e.toString());
            staffs = null;
        } finally {
            return staffs;
        }
    }

    public void setPartitionStaffs(Map<Boolean, List<Staff>> partitionStaffs) {
        this.partitionStaffs = partitionStaffs;
    }

    public void insertStaffsIntoDatabase() {
        if (partitionStaffs != null) {
            int count = 0;
            int maxTry = 3;

            while (true) {
                try (SqlWrapper sqlWrapper = new SqlWrapper();) {
                    sqlWrapper.openConnection();
                    List<Staff> staffs = partitionStaffs.get(true);
                    String insertQuery = SqlQueryGenerator.generateInsertQuery("staff");
                    PreparedStatement statement = sqlWrapper.getConnection().prepareStatement(insertQuery);
                    executeInsertStatementWithStaffs(staffs, statement);
                    break;
                } catch (ClassNotFoundException e) {
                    System.out.println(e.toString());
                    break;
                } catch (SQLException e) {
                    String message = e.toString();
                    System.out.println(message);
                    if (message.contains("connection")) {
                        if (++count >= maxTry)
                            break;
                    } else
                        break;
                }
            }
        }
    }

    private void executeInsertStatementWithStaffs(List<Staff> staffs, PreparedStatement statement)
            throws SQLException {

        int quota = 1000;
        for (int i = 0; i < staffs.size(); ++i) {
            statement.setString(1, staffs.get(i).getFirstName());
            statement.setString(2, staffs.get(i).getMiddleName());
            statement.setString(3, staffs.get(i).getLastName());
            statement.setString(4, staffs.get(i).getDateOfBirth());
            statement.setString(5, staffs.get(i).getGender());
            statement.setString(6, staffs.get(i).getPhone());
            statement.setString(7, staffs.get(i).getAddress());

            statement.addBatch();
            if (i == quota  // reach the limit for each batch
                    || i == staffs.size() - 1) // reach the last item
                statement.executeBatch();
        }
    }

    public void writeLogsForFailRecords() {
        if (partitionStaffs != null) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(
                    new FileWriter("log.txt"));) {
                List<Staff> staffs = partitionStaffs.get(false);
                bufferedWriter.write("Failed records:\n");
                for (Staff s : staffs) {
                    bufferedWriter.write(s.toString());
                    bufferedWriter.write('\n');
                }
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
    }
}
