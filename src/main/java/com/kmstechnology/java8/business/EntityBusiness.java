package com.kmstechnology.java8.business;

import com.kmstechnology.java8.validator.CompositeValidators;

import java.util.HashMap;
import java.util.Map;

public abstract class EntityBusiness {
    protected Map<String, CompositeValidators> validatorsByComposite;

    public EntityBusiness() {
        validatorsByComposite = new HashMap<>();
    }

    public abstract boolean validateEntity(Object entity);
}
