package com.kmstechnology.java8.business;

import com.kmstechnology.java8.entity.Staff;
import com.kmstechnology.java8.validator.AddressCompositeValidators;
import com.kmstechnology.java8.validator.CompositeValidators;
import com.kmstechnology.java8.validator.DateFormatCompositeValidators;
import com.kmstechnology.java8.validator.GenderCompositeValidators;
import com.kmstechnology.java8.validator.MiddleNameCompositeValidators;
import com.kmstechnology.java8.validator.PhoneCompositeValidators;
import com.kmstechnology.java8.validator.StrictNameCompositeValidators;

public class StaffBusiness extends EntityBusiness {

    public StaffBusiness() {
        addCompositeValidators();
    }

    private void addCompositeValidators() {
        addFirstAndLastNameValidators();
        addMiddleNameValidators();
        addDateFormatValidators();
        addGenderValidators();
        addPhoneFormatValidators();
        addAddressValidators();
    }

    private void addFirstAndLastNameValidators() {
        CompositeValidators strictNameValidators = new StrictNameCompositeValidators();
        strictNameValidators.addDefaultValidators();
        validatorsByComposite.put(strictNameValidators.getValidatorName(), strictNameValidators);
    }

    private void addMiddleNameValidators() {
        CompositeValidators middleNameValidators = new MiddleNameCompositeValidators();
        middleNameValidators.addDefaultValidators();
        validatorsByComposite.put(middleNameValidators.getValidatorName(), middleNameValidators);
    }

    private void addDateFormatValidators() {
        CompositeValidators dateFormatValidators = new DateFormatCompositeValidators();
        dateFormatValidators.addDefaultValidators();
        validatorsByComposite.put(dateFormatValidators.getValidatorName(), dateFormatValidators);
    }

    private void addGenderValidators() {
        CompositeValidators genderValidators = new GenderCompositeValidators();
        genderValidators.addDefaultValidators();
        validatorsByComposite.put(genderValidators.getValidatorName(), genderValidators);
    }

    private void addPhoneFormatValidators() {
        CompositeValidators phoneValidators = new PhoneCompositeValidators();
        phoneValidators.addDefaultValidators();
        validatorsByComposite.put(phoneValidators.getValidatorName(), phoneValidators);
    }

    private void addAddressValidators() {
        CompositeValidators addressValidators = new AddressCompositeValidators();
        addressValidators.addDefaultValidators();
        validatorsByComposite.put(addressValidators.getValidatorName(), addressValidators);
    }

    public static boolean validateStaffByBusiness(Staff staff) {
        if (staff == null)
            return false;

        EntityBusiness business = new StaffBusiness();
        return business.validateEntity(staff);
    }

    @Override
    public boolean validateEntity(Object entity) {
        Staff staff = (Staff) entity;
        if (staff == null)
            return false;

        boolean validateFirstName = this.validateFirstName(staff);
        boolean validateMiddleName = this.validateMiddleName(staff);
        boolean validateLastName = this.validateLastName(staff);
        boolean validateDateOfBirth = this.validateDateOfBirth(staff);
        boolean validateGender = this.validateGender(staff);
        boolean validatePhone = this.validatePhone(staff);
        boolean validateAddress = this.validateAddress(staff);

        return validateFirstName && validateMiddleName && validateLastName && validateDateOfBirth
                && validateGender && validatePhone && validateAddress;
    }

    private boolean validateFirstName(Staff staff) {
        return validatorsByComposite.get(StrictNameCompositeValidators.class.getName())
                .validateText(staff.getFirstName());
    }

    private boolean validateMiddleName(Staff staff) {
        return validatorsByComposite.get(MiddleNameCompositeValidators.class.getName())
                .validateText(staff.getMiddleName());
    }

    private boolean validateLastName(Staff staff) {
        return validatorsByComposite.get(StrictNameCompositeValidators.class.getName())
                .validateText(staff.getLastName());
    }

    private boolean validateDateOfBirth(Staff staff) {
        return validatorsByComposite.get(DateFormatCompositeValidators.class.getName())
                .validateText(staff.getDateOfBirth());
    }

    private boolean validateGender(Staff staff) {
        return validatorsByComposite.get(GenderCompositeValidators.class.getName())
                .validateText(staff.getGender());
    }

    private boolean validatePhone(Staff staff) {
        return validatorsByComposite.get(PhoneCompositeValidators.class.getName())
                .validateText(staff.getPhone());
    }

    private boolean validateAddress(Staff staff) {
        return validatorsByComposite.get(AddressCompositeValidators.class.getName())
                .validateText(staff.getAddress());
    }
}
