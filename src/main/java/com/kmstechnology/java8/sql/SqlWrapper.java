package com.kmstechnology.java8.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlWrapper implements AutoCloseable {
    Connection connection;

    public SqlWrapper() {
        connection = null;
    }

    public Connection getConnection() {
        return connection;
    }

    public void openConnection()
            throws ClassNotFoundException, SQLException {
        if (connection != null)
            connection = null;

        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/mydatabase", "root", "admin");
    }

    public void closeConnection() throws SQLException {
        if (connection != null)
            connection.close();
        connection = null;
    }

    @Override
    public void close() throws SQLException {
        closeConnection();
    }
}
