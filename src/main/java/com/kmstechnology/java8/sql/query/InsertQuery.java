package com.kmstechnology.java8.sql.query;

import com.kmstechnology.java8.sql.table.DatabaseTable;

import java.util.List;
import java.util.Optional;

public class InsertQuery extends SqlQuery {
    @Override
    public String generateQueryString(String table) {
        Optional<List<String>> columns = Optional.ofNullable(getColumns(table));

        if (columns.isPresent())
            return generateStringFromColumns(table, columns.get());

        return null;
    }

    private List<String> getColumns(String table) {
        return DatabaseTable.getColumnsFromTable(table);
    }

    private String generateStringFromColumns(String table, List<String> columns) {
//        "insert into staff (first_name, middle_name, last_name, date_of_birth," +
//                        "gender, phone, address) values (?, ?, ?, ?, ?, ?, ?)";
        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append("insert into ");
        stringBuilder.append(table);
        stringBuilder.append(" (");

        insertColumnsIntoQuery(columns);
        insertValuesIntoQuery(columns.size());

        return stringBuilder.toString();
    }

    private void insertColumnsIntoQuery(List<String> columns) {
        for (int i = 0; i < columns.size(); ++i) {
            stringBuilder.append(columns.get(i));
            if (i < columns.size() - 1)
                stringBuilder.append(", ");
            else
                stringBuilder.append(") values (");
        }
    }

    private void insertValuesIntoQuery(int length) {
        for (int i = 0; i < length; ++i) {
            stringBuilder.append("?");
            if (i < length - 1)
                stringBuilder.append(", ");
            else
                stringBuilder.append(")");
        }
    }
}
