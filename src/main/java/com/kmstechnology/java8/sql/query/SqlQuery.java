package com.kmstechnology.java8.sql.query;

public abstract class SqlQuery {
    StringBuilder stringBuilder;

    public SqlQuery() {
        stringBuilder = new StringBuilder();
    }

    public abstract String generateQueryString(String table);
}
