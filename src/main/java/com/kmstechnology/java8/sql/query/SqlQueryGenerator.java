package com.kmstechnology.java8.sql.query;

public class SqlQueryGenerator {
    public static String generateInsertQuery(String table) {
        SqlQuery query = new InsertQuery();
        return query.generateQueryString(table);
    }
}
