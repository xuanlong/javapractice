package com.kmstechnology.java8.sql.table;

import java.util.ArrayList;
import java.util.List;

public abstract class Table {
    String name;
    List<String> columns;

    public Table() {
        columns = new ArrayList<>();
    }

    protected abstract void addAllColumns();

    protected void addColumn(String column) {
        if (!columns.stream().anyMatch(c -> c.equals(column)))
            columns.add(column);
    }

    public String getName() {
        return name;
    }

    public List<String> getColumns() {
        return columns;
    }
}
