package com.kmstechnology.java8.sql.table;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseTable {
    private static Map<String, Table> tableByName;

    static {
        tableByName = new HashMap<>();
    }

    public static List<String> getColumnsFromTable(String table) {
        createNonExistTable(table);

        Table t = tableByName.get(table);
        if (t == null)
            return null;

        return t.getColumns();
    }

    private static void createNonExistTable(String key) {
        Table table = tableByName.get(key);
        if (table == null) {
            table = createTable(key);
            tableByName.put(key, table);
        }
    }

    private static Table createTable(String tableName) {
        Table table = null;

        switch (tableName) {
            case "staff": {
                table = new StaffTable();
                break;
            }
        }
        return table;
    }
}
