package com.kmstechnology.java8.sql.table;

public class StaffTable extends Table {
    public StaffTable() {
        this.name = "staff";

        addAllColumns();
    }

    @Override
    protected void addAllColumns() {
        addColumn("first_name");
        addColumn("middle_name");
        addColumn("last_name");
        addColumn("date_of_birth");
        addColumn("gender");
        addColumn("phone");
        addColumn("address");
    }
}
